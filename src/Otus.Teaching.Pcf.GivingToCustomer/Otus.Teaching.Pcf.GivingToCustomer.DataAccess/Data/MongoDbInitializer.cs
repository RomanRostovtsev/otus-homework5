﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {

        private MongoDataContext _dataContext;
        public MongoDbInitializer(MongoDataContext dataContext) { 
            _dataContext = dataContext;
        }
        public void InitializeDb()
        {
            _dataContext.RecreateDB();
            
            _dataContext.GetCollection<Preference>().InsertMany(FakeDataFactory.Preferences);
            _dataContext.GetCollection<Customer>().InsertMany(FakeDataFactory.Customers);
        }

        public void ClearDb()
        {
            _dataContext.DropDB();
        }
    }
}
