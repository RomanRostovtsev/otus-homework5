﻿using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoDataContext
    {
        private readonly string _dbName;
        private readonly string _connectionString;
        private IMongoDatabase _db;
        private Dictionary<Type, string> _collectionNames = new Dictionary<Type, string>();
        private Dictionary<Type, object> _collections = new Dictionary<Type, object>();

        public MongoDataContext(string connectionString, string dbName) 
        {
            _dbName = dbName;
            _connectionString = connectionString;            
            RegisterCollections();
        }

        private void RegisterCollections()
        {
            RegisterCollection(typeof(PromoCode), "promocodes");
            RegisterCollection(typeof(Customer), "customers");
            RegisterCollection(typeof(Preference), "preferences");
            
        }

        private void RegisterCollection(Type itemType, string collectionName)
        {
            _collectionNames[itemType] = collectionName;
        }

        private IMongoDatabase DB { 
            get{
                if (_db == null)
                {
                    var mongoClient = new MongoClient(_connectionString);
                    _db = mongoClient.GetDatabase(_dbName);
                }
                return _db;
            } 
        }

        public IMongoCollection<TEntity> GetCollection<TEntity>() where TEntity: class {
            if (_collections.TryGetValue(typeof(TEntity), out object result)) {
                return (IMongoCollection<TEntity>) result;
            }
            if(!_collectionNames.TryGetValue(typeof(TEntity), out string collectionName))
            {
                return null;
            }
            result = DB.GetCollection<TEntity>(collectionName);
            _collections[typeof(TEntity)] = result;
            return (IMongoCollection<TEntity>) result;
        } 

        public async Task<object> GetEntityAsync(Type entityType, Guid id)
        {
            string collectionName = _collectionNames[entityType];
            var collection = DB.GetCollection<BsonDocument>(collectionName);
            var document = await collection.Find(d => d["_id"].AsGuid == id).FirstOrDefaultAsync();
            if (document == null) return null;
            return BsonSerializer.Deserialize(document, entityType);
        }
        
        public async Task<List<object>> GetAllEntitiesAsync(Type entityType)
        {
            string collectionName = _collectionNames[entityType];
            var collection = DB.GetCollection<BsonDocument>(collectionName);
            var documents = await collection.Find(d => true).ToListAsync();
            List<object> result = new List<object>();
            foreach (var document in documents)
            {
                result.Add(BsonSerializer.Deserialize(document, entityType));
            }
            return result;
        }

        public void RecreateDB()
        {
            var mongoClient = new MongoClient(_connectionString);            
            mongoClient.DropDatabase(_dbName);
            _db = mongoClient.GetDatabase(_dbName);
            foreach(var pair in _collectionNames)
            {
                _db.CreateCollection(pair.Value);
            }
        }

        public void DropDB()
        {
            if(_db != null )
            {
                _db.Client.DropDatabase(_dbName);
                _db = null;
            }
        }
    }
}
