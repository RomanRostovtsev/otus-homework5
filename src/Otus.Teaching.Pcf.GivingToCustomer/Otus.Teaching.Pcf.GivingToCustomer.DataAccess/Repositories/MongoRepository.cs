﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private MongoDataContext _dataContext;
        private IMongoCollection<T> _collection;
        public MongoRepository(MongoDataContext dataContext) {
            _collection = dataContext.GetCollection<T>();
            _dataContext = dataContext;
        }

        public Task AddAsync(T entity)
        {
            return _collection.InsertOneAsync(entity);
        }

        public Task DeleteAsync(T entity)
        {
            return _collection.DeleteOneAsync(x => x.Id == entity.Id);            
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var list = await _collection.Find(x => true).ToListAsync();
            return list;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            T entity = await _collection.Find(x =>x.Id == id).FirstOrDefaultAsync();
            if(entity != null) await LoadChildrenAsync(entity);
            return entity;
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            T entity = await _collection.Find(predicate).FirstOrDefaultAsync();
            if (entity != null) await LoadChildrenAsync(entity);
            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var list = await _collection.Find(x => ids.Contains(x.Id)).ToListAsync();
            return list;
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            var list = await _collection.Find(predicate).ToListAsync();
            foreach(var item in list)
            {
                await LoadChildrenAsync(item);
            }
            return list;
        }

        public Task UpdateAsync(T entity)
        {
            return _collection.ReplaceOneAsync(x => x.Id == entity.Id, entity);
        }

        private static bool IsCollectionType(Type type, out Type itemType)
        {
            itemType = null;
            if (!type.IsGenericType) return false;
            if (type.IsGenericTypeDefinition) return false;
            if (typeof(ICollection<>) == type.GetGenericTypeDefinition())
            {
                itemType = type.GenericTypeArguments[0];
                return true;
            }

            return false;
        }

        private async Task LoadChildrenAsync(BaseEntity entity)
        {
            await SetupManyToManyOtherSideAsync(entity);
            await SetupEntitiesByIdsAsync(entity);
        }

        private async Task SetupManyToManyOtherSideAsync(BaseEntity entity) {
            Type type = entity.GetType();
            foreach (var prop in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var attribute = prop.GetCustomAttribute<JoinedEntityAttribute>();
                if (attribute == null) continue;
                if (!IsCollectionType(prop.PropertyType, out var itemType)) continue;
                dynamic collection = prop.GetValue(entity);
                if (collection == null || collection.Count != 0) continue;
                PropertyInfo backProperty = itemType.GetProperties().FirstOrDefault(prop => prop.PropertyType == type);
                if (backProperty == null) continue;
                PropertyInfo backIdProperty = itemType.GetProperties().FirstOrDefault(prop => prop.Name == backProperty.Name + "Id");
                if (backIdProperty == null) continue;
                List<object> allTargetEntities = await _dataContext.GetAllEntitiesAsync(attribute.JoinedEntity);
                foreach (var targetEntity in allTargetEntities)
                {
                    dynamic linkingRecords = targetEntity.GetType().GetProperty(attribute.PropertyName).GetValue(targetEntity);
                    if (linkingRecords != null)
                    {
                        foreach (var linkingRecord in linkingRecords)
                        {
                            if (linkingRecord != null)
                            {
                                if ((Guid)backIdProperty.GetValue(linkingRecord) == entity.Id)
                                {
                                    collection.Add(linkingRecord);
                                }
                            }
                        }
                    }
                }
            }
        }

        private async Task SetupEntitiesByIdsAsync(object entity) { 
            Type type = entity.GetType();
            foreach (var prop in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (IsCollectionType(prop.PropertyType, out var itemType))
                {
                    dynamic collection = prop.GetValue(entity);
                    if (collection != null)
                    {
                        foreach (var item in collection)
                        {
                            if (item != null) await SetupEntitiesByIdsAsync(item);
                        }
                    }
                }
                else if (prop.GetCustomAttribute<BsonIgnoreAttribute>() != null)
                {
                    var idProp = type.GetProperty(prop.Name + "Id", BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                    if (idProp != null && idProp.PropertyType == typeof(Guid))
                    {
                        Guid id = (Guid)idProp.GetValue(entity);
                        object childValue = await _dataContext.GetEntityAsync(prop.PropertyType, id);
                        prop.SetValue(entity, childValue);
                    }
                }
            }
        }

    }
}
