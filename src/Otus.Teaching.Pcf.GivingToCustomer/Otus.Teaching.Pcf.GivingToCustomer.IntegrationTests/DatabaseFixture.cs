﻿using System;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class DatabaseFixture: IDisposable
    {
#if POSTGRES
        private readonly EfTestDbInitializer _efTestDbInitializer;
        
        public DatabaseFixture()
        {
            DbContext = new TestDataContext();

            _efTestDbInitializer= new EfTestDbInitializer(DbContext);
            _efTestDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _efTestDbInitializer.CleanDb();
        }

        public TestDataContext DbContext { get; private set; }
#else
        private readonly MongoDbInitializer _dbInitializer;
        private readonly MongoDataContext _dbContext;
        public DatabaseFixture()
        {
            _dbContext = new MongoDataContext("mongodb://root:example@localhost:27017/?authMechanism=SCRAM-SHA-256", "promocode_factory_giving_to_customer");
            _dbInitializer = new MongoDbInitializer(_dbContext);
            _dbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _dbInitializer.ClearDb();
        }

        public MongoDataContext DbContext { get { return _dbContext; } }

#endif
    }
}