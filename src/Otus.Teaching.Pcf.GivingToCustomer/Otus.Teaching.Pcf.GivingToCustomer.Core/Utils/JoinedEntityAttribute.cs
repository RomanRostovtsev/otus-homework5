﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Utils
{
    [AttributeUsage(AttributeTargets.Property)]
    public class JoinedEntityAttribute : Attribute
    {
        public JoinedEntityAttribute(Type joinedEntity, string propertyName) {
            JoinedEntity= joinedEntity;
            PropertyName= propertyName;
        }

        public Type JoinedEntity { get; set; }
        public string PropertyName { get; set; }   
    }
}
