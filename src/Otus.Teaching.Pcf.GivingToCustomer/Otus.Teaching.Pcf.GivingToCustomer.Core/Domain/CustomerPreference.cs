﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class CustomerPreference
    {
        public Guid CustomerId { get; set; }
        
        [BsonIgnore]             
        public virtual Customer Customer { get; set; }

        public Guid PreferenceId { get; set; }

        [BsonIgnore]
        public virtual Preference Preference { get; set; }
    }
}