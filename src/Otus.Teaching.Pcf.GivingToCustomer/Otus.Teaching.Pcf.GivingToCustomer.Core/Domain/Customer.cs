﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Utils;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [BsonIgnore]
        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public virtual ICollection<CustomerPreference> Preferences { get; set; } = new List<CustomerPreference>();

        [BsonIgnore]
        [JoinedEntity(typeof(PromoCode), "Customers")]
        public virtual ICollection<PromoCodeCustomer> PromoCodes { get; set; } = new List<PromoCodeCustomer>();
    }
}